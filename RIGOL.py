import visa


class RigolDP832A:
    def __init__(self, resource_path):
        self.rm = visa.ResourceManager()
        self.inst = self.rm.open_resource(resource_path)

    def get_info(self):
        return self.inst.query("*IDN?")

    def channel_on(self, channel_number):
        self.inst.write(':OUTP CH{channel},ON'.format(channel=channel_number))

    def channel_off(self, channel_number):
        self.inst.write(':OUTP CH{channel},OFF'.format(channel=channel_number))

    def reset(self):
        self.inst.write("*RST")

    def close_instrument(self):
        self.inst.close

    def set_current(self, channel_number, current):
        self.inst.write(':SOUR{channel}:CURR {curr}'.format(channel=channel_number, curr=current))

    def set_voltage(self, channel_number, voltage):
        self.inst.write(':SOUR{channel}:VOLT {volt}'.format(channel=channel_number, volt=voltage))

    def get_current(self, channel_number):
        return self.inst.query(':MEAS:CURR? CH{channel}'.format(channel=channel_number))

    def get_voltage(self, channel_number):
        return self.inst.query(':MEAS:VOLT? CH{channel}'.format(channel=channel_number))


class RigolDG4102:
    def __init__(self, resource_path):
        self.rm = visa.ResourceManager()
        self.inst = self.rm.open_resource(resource_path)

    def get_info(self):
        return self.inst.query('*IDN?')

    def set_frequency(self, channel_number, frequency):
        self.inst.write(':SOUR{channel}:FREQ {freq}'.format(channel=channel_number, freq=frequency))

    def set_amplitude(self, channel_number, amplitude):
        pass

    def set_sin_waveform(self, channel_number, frequency, amplitude, offset, phase):
        self.inst.write(':SOUR{channel}:APPL:SIN {freq},{ampl},{off},{phase}'.format(channel=channel_number,
                                                                                        off=offset,
                                                                                        freq = frequency,
                                                                                        ampl = amplitude,
                                                                                        phase = phase))


class RigolDS1104:
    def __init__(self, resource_path):
        self.rm = visa.ResourceManager()
        self.inst = self.rm.open_resource(resource_path)

    def get_info(self):
        return self.inst.query('*IDN?')

    def calibrate(self):
        self.inst.write(':CAL:STAR')
        self.inst.close

    def autosetup(self):
        pass

    def set_mode(self, mode = 'auto' ):
        if not isinstance(mode, str):
            mode = str(mode)
        self.inst.write(':TRIG:SWE {mode}'.format(mode=mode))

    def set_triger_level(self, level):
        self.inst.write(':TRIGger:EDGe:LEVel {level}'.format(level=level))

    def set_vertical_scale(self, channel, scale):
        self.inst.write('CHAN{channel}:SCALE {scale}'.format(channel=channel, scale=scale))

    def set_horizontal_scale(self, scale):
        self.inst.write(':TIM:MAIN:SCAL {scale}'.format(scale=scale))

    def get_vpp(self, channel):
        return self.inst.query(':MEAS:ITEM? VPP,CHAN{channel}'.format(channel=channel))
