import RIGOL as RigolDevices

power_supply_path = 'TCPIP::192.168.1.198::INSTR'
oscilloscope_path = 'TCPIP::192.168.1.197::INSTR'
generator_path    = 'TCPIP::192.168.1.196::INSTR'

_power_supply = RigolDevices.RigolDP832A(power_supply_path)
_oscilloscope = RigolDevices.RigolDS1104(oscilloscope_path)
_generator    = RigolDevices.RigolDG4102(generator_path)

if __name__ == '__main__':
    print(_power_supply.get_info())
    print(_oscilloscope.get_info())
    print(_generator.get_info())

    print("end")
